package com.bottlerocketapps.launchintentapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	View formContainer;
	EditText edit;
	Button submit;
	TextView error;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		
		setupForm();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	
	
	//
	//	FORM
	//
	
	private void setupForm() {
		formContainer = findViewById(R.id.form_layout);
		edit = (EditText) findViewById(R.id.entry);
		submit = (Button) findViewById(R.id.submit);
		error = (TextView) findViewById(R.id.error);
		submit.setOnClickListener(new OnClickListener() {
			
			@Override public void onClick(View v) {
				onSubmit();
			}
		});
	}
	
	private void onSubmit() {
		String protocol = edit.getText().toString();
		if (!protocol.contains("://")) {
			protocol += "://";
		}
		submit(protocol);
	}
	
	private void submit(String url) {
		Intent intent = new Intent(this, WebViewActivity.class);
		intent.putExtra(WebViewActivity.STATE_URL, url);
		startActivity(intent);
		
	}
}
