package com.bottlerocketapps.launchintentapp;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class WebViewActivity extends Activity {
	
	//
	//	LIFECYCLE
	//
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		onCreateState(savedInstanceState);
		setContentView(R.layout.webview_activity);
		setupWebView();
	}
	
	
	
	//
	//	ARGS AND STATE
	//
	
	public static final String STATE_URL = "url";
	
	private void onCreateState(Bundle state) {
		if (state == null) {
			state = getIntent().getExtras();
		}

		mUrl = state.getString(STATE_URL);
	}
	
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(STATE_URL, mUrl);
	};
	
	
	
	//
	//	WEBVIEW
	//
	
	WebView webView;
	String mUrl;
	
	private void setupWebView() {
		webView = (WebView) findViewById(R.id.webview);
		String httpWithLink =
				"<http>" +
					"<p>" +
						"<a href = \"" + mUrl + "\">" + 
							mUrl + 
						"</a>" +
					"</p>" +
				"</http>";
		webView.loadData(httpWithLink, "text/html", "UTF-8");
	}
}
